<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Bear Arms</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="img/favicon.png" />
    <link rel="stylesheet" href="node_modules/normalize.css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i|Roboto+Condensed:700" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css?<?php echo time(); ?>">
  </head>
  <body>
    <div class="swipe">

    </div>
    <div class="l-site">
      <a class="branding" href="#" data-nav="1">
        <img src="img/bear.svg" alt="Bear">
        <div class="branding-text">
          <img src="img/beararms.svg" alt="Bear Arms, Meubles sur mesure">
        </div>
      </a>

      <nav class="nav">
        <a href="#" class="nav-burger" data-nav-trigger>
          <div class="nav-burger-inner"></div>
        </a>
        <ul data-nav-wrapper>
          <li><a href="#voir-my-projects" data-nav="2" data-nav-link>Voir my projects</a></li>
          <li><a href="#comment-je-work" data-nav="3" data-nav-link>Comment je work</a></li>
          <li><a href="#travailler-together" data-nav="4" data-nav-link>Travailler together</a></li>
        </ul>
      </nav>

      <div class="l-pages" data-pages>
        <div class="l-page t-home is-active" data-page="home" data-page-title="Bear Arms">
          <div class="l-content">
            <div class="l-row">
              <div class="l-col-intro-left">
                <dl class="intro">
                  <dt>Hello, je suis</dt>
                  <dd>Chad Gunter</dd>
                  <dt>My french is comme ci comme ça, but I am good at building des</dt>
                  <dd>
                    <ul class="intro-slide">
                      <li class="is-active">tables<span> de conférence</span></li>
                      <li>chaises</li>
                      <li>bureaux</li>
                      <li>armoires</li>
                    </ul>
                  </dd>
                  <dt>I like to work with </dt>
                  <dd>bois de la région</dd>
                  <dt>Mon atelier is in</dt>
                  <dd>Marly (Fribourg)</dd>
                </dl>
              </div>
              <div class="l-col-intro-right">
                <img class="img" src="img/content/table.jpg" alt="table">
              </div>
            </div>
          </div>
          <a class="next" href="#voir-my-projects" data-nav="2" data-nav-link>Voir my<br>projects</a>
        </div>
        <div class="l-page t-product" data-page="voir-my-projects" data-page-title="Voir my projects | Bear Arms">
          <div class="l-sections">
            <div class="l-section is-active" data-section="1">
              <div class="l-product">
                <div class="l-row">
                  <div class="l-col-third">
                    <ul class="tags">
                      <li class="is-active">business</li>
                      <li>epoxy</li>
                      <li class="is-active">table</li>
                      <li class="is-active">metal</li>
                      <li class="is-active">privé</li>
                      <li class="is-active">décoration</li>                 
                    </ul>
                    <h1>Table basse</h1>
                  </div>
                </div>
                <div class="l-row">
                  <div class="l-col-large">
                    <img class="img" src="img/content/table.jpg" alt="table">
                    <img class="img-large" src="img/content/table.jpg" alt="table" data-sideimage="1">
                  </div>
                </div>
              </div>
            </div>
            <div class="l-section" data-section="2">
              <div class="l-product">
                <div class="l-row">
                  <div class="l-col-third">
                    <ul class="tags">
                      <li class="is-active">business</li>
                      <li>epoxy</li>
                      <li class="is-active">table</li>
                      <li class="is-active">metal</li>
                      <li class="is-active">privé</li>
                      <li class="is-active">décoration</li>                 
                    </ul>
                    <h1>Armoire</h1>
                  </div>
                </div>
                <div class="l-row">
                  <div class="l-col-large">
                    <img class="img" src="img/content/desk.jpg" alt="table">
                    <img class="img-large" src="img/content/desk.jpg" alt="table" data-sideimage="2">
                  </div>
                </div>
              </div>
            </div>
            <div class="l-section" data-section="3">
              <div class="l-product">
                <div class="l-row">
                  <div class="l-col-third">
                    <ul class="tags">
                      <li class="is-active">business</li>
                      <li>epoxy</li>
                      <li class="is-active">table</li>
                      <li class="is-active">metal</li>
                      <li class="is-active">privé</li>
                      <li class="is-active">décoration</li>                 
                    </ul>
                    <h1>Table de conférence</h1>
                  </div>
                </div>
                <div class="l-row">
                  <div class="l-col-large">
                    <img class="img" src="img/content/chair.jpg" alt="table">
                    <img class="img-large" src="img/content/chair.jpg" alt="table" data-sideimage="3">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <a class="next" href="#comment-je-work" data-nav="3" data-nav-link>Comment<br>je work</a>
        </div>
        <div class="l-page" data-page="comment-je-work" data-page-title="Comment je work | Bear Arms">
          <div class="l-content">
            <div class="l-row">
              <div class="l-col-third">
                <h1 class="content-title">Comment je work</h1>
                <img class="img-large" src="img/content/table.jpg" alt="table">
                <div class="content-body">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volut.</p>
                  <ul>
                    <li>> bois de la région</li>
                    <li>> résine epoxy</li>
                    <li>> utilisation rare de vis ou de clou</li>
                  </ul>
                </div>
                <img class="content-img t-right" src="img/content/cross.jpg" alt="cross">
                <img class="content-img t-left" src="img/content/chair.jpg" alt="chair">
              </div>
            </div>
          </div>
          <a class="next" href="#travailler-together" data-nav="4" data-nav-link>Travailler<br>together</a>
        </div>
        <div class="l-page" data-page="travailler-together" data-page-title="Travailler together | Bear Arms">
          <div class="l-content">
            <div class="l-row">
              <div class="l-col-third">
                <h1 class="content-title">Travailler together</h1>
                <img class="img-large" src="img/content/table.jpg" alt="table">
                <div class="content-body">
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volut.</p>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volut.</p>
                </div>
                <img class="content-img t-right" src="img/content/cross.jpg" alt="cross">
                <img class="content-img t-left" src="img/content/chair.jpg" alt="chair">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/animejs/anime.min.js"></script>
    <script src="node_modules/lethargy/lethargy.min.js"></script>
    <script src="https://unpkg.com/history"></script>
    <script src="js/plugins/mobileNav.js?<?php echo time(); ?>"></script>
    <script src="js/plugins/desktopNav.js?<?php echo time(); ?>"></script>
    <script src="js/plugins/fullpageScroll.js?<?php echo time(); ?>"></script>
    <script src="js/plugins/slide.js?<?php echo time(); ?>"></script>
    <script src="js/main.js?<?php echo time(); ?>"></script>
  </body>
</html>