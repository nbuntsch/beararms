fixedAside = false;

$('[data-nav-trigger]').mobileNav();
// $('[data-nav]').desktopNav();
$('.l-page.t-product').fullPageScroll();
$('.intro-slide').slide();

var pageId = window.location.hash.substr(1);

if (pageId != '') {
  var $page = $('[data-page=' + pageId + ']');
  var offset = $page.index() * -100 + 'vw';
  $('[data-pages]').css('transform', 'translateX(' + offset + ')');
  $('.branding-text img').css('transform', 'translateX(-120%)');
  $('[data-nav][href="#' + pageId + '"]').addClass('is-active');
}

var createBrowserHistory = window.History.createBrowserHistory;
const history = createBrowserHistory();

history.listen((location, action) => {
  var pageId = location.hash.substr(1);

  if (pageId === '') {
    pageId = 'home';
  }

  var $page = $('[data-page=' + pageId + ']');
  var offset = $page.index() * -100 + 'vw';

  document.title = $page.data('page-title');

  anime({
    targets: '[data-pages]',
    translateX: offset,
    duration: 1000,
    easing: 'easeInOutQuart',
  });

  $('[data-nav], [data-page]').removeClass('is-active');
  $('[data-page=' + pageId + ']').addClass('is-active');

  if (pageId === 'home') {
    anime({
      targets: '.branding-text img',
      translateX: '0',
      duration: 500,
      delay: 500,
    });
  } else {
    $('[data-nav][href="#' + pageId + '"]').addClass('is-active');

    anime({
      targets: '.branding-text img',
      translateX: '-120%',
      duration: 500,
      delay: 500
    });
  }
});