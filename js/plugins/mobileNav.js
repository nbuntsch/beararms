$.fn.mobileNav = function() {
  $(this).click(function(e) {
    e.preventDefault();
    toggleNav();
  })

  $('[data-nav-link]').click(function() {
    toggleNav();
  });
}

function toggleNav() {
  $('[data-nav-wrapper], [data-nav-trigger]').toggleClass('is-active');
}