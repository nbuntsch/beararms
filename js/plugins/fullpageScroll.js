$.fn.fullPageScroll = function() {
  var lethargy = new Lethargy();
  var breakpoint = 768;

  $(this).each(function(i,e) {
    var $page = $(e);
    var isLarge = false;
    var $current = $page.find('.l-section.is-active');

    if ($(window).width() >= breakpoint) {
      isLarge = true;
      toLarge($page, $current);
    }

    $(window).resize(function() {
      if (isLarge === false && $(window).width() >= breakpoint) {
        isLarge = true;
        toLarge($page, $current);
      } else if (isLarge === true && $(window).width() < breakpoint) {
        isLarge = false;
        toSmall($page);
      }
    });

    var running = false;
    $page.bind('mousewheel DOMMouseScroll wheel MozMousePixelScroll', function(e){
      if ($(window).width() >= breakpoint) {
        e.preventDefault()
        e.stopPropagation();

        if(running === false && lethargy.check(e) !== false) {
          var down = e.originalEvent.deltaY > 0;
          var move = false;
          
          if (down === true && $current.next().length > 0) {
            var fromIndex = $current.data('section');
            $current = $current.next();
            move = true;
          } else if (down === false && $current.prev().length > 0) {
            var fromIndex = $current.data('section');
            $current = $current.prev();
            move = true;
          }

          if (move === true) {
            running = true;
            $page.find('.l-section').removeClass('is-active');
            $current.addClass('is-active');
            var offset = $current.index() * -100 + 'vh';

            anime({
              targets: $page.find('.l-sections')[0],
              translateY: offset,
              duration: 1000,
              easing: 'easeInOutQuart',
              complete: function() {
                setTimeout(function(){
                  running = false;
                }, 100);              
              }
            });

            anime({
              targets: '[data-sideimage="' + fromIndex + '"].is-fixed',
              opacity: 0,
              duration: 1000,
              easing: 'easeInOutQuart'
            });

            anime({
              targets: '[data-sideimage="' + $current.data('section') + '"].is-fixed',
              opacity: 1,
              duration: 1000,
              easing: 'easeInOutQuart'
            });
          }
        }    
      }
    });
  });
}

function toLarge ($page, $current) {
  $page
    .find('[data-sideimage]')
    .hide()
    .clone()
    .css('opacity', 0)
    .show()
    .addClass('is-fixed')
    .appendTo($page)
    .eq($current.index()).css('opacity', 1);
}

function toSmall ($page) {
  $page
    .find('[data-sideimage].is-fixed')
    .remove();

  $page
    .find('[data-sideimage]')
    .show();
}