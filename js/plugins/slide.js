$.fn.slide = function() {
  slide($(this));
}

function slide($slide) {
  setTimeout(function() {
    var $current = $slide.find('li.is-active');
    var $next = $current.next();

    if ($next.length === 0) {
      $next = $slide.find('li').first();
    }

    anime({
      targets: $current[0],
      top: '-100%',
      duration: 400,
      easing: 'easeInOutQuart',
      complete: function() {
        $current.removeClass('is-active');
        $current.css('top', '100%');
      }
    });

    anime({
      targets: $next[0],
      top: '0',
      duration: 400,
      easing: 'easeInOutQuart',
      complete: function() {
        $next.addClass('is-active');
      }
    });
  
    slide($slide);
  }, 3000);
}