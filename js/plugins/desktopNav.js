$.fn.desktopNav = function() {
  $(this).on('click', function() {
    var id = $(this).data('nav');
    var offset = (id - 1) * -100 + 'vw';
  
    anime({
      targets: '[data-pages]',
      translateX: offset,
      duration: 1000,
      easing: 'easeInOutQuart',
    });

    $('[data-nav], [data-page]').removeClass('is-active');
    $('[data-page=' + id + ']').addClass('is-active');

    if (id === 1) {
      anime({
        targets: '.branding-text img',
        translateX: '0',
        duration: 500,
        delay: 500,
      });
    } else {
      $('[data-nav=' + id + ']').addClass('is-active');

      anime({
        targets: '.branding-text img',
        translateX: '-120%',
        duration: 500,
        delay: 500
      });
    }
  });
}